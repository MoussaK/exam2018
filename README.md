# Examen de Java et Programmation Réseau, Télécom 2, Sup-Galilée, 2018
# Réponse aux questions
## Exo 1 
### Question 1
	* Un **bean** est une classe contenant les proprietés (privées), les setters et getters associés à ces proprietés et un constructeur vide
### Question 2
	* Il faut ajouter la méthode toString()
### Question 3
	* Deux objets sont égaux au sens de equals si ils ont mêmes hash code, il faut générer la méthode haschcode à chaque fois que l'on surcharge equals
### Question 4
	* Pour comparer deux objets en java on fais implémenter l'interface Comparable par la classe et on surcharge compareTo()
	* On peut égalemnt passer par un la méthode sort() de la classe factory Collections
### Question 6
	* On utilise Java I/O pour ça; On utilise un FileReader qui délègue la lecture du fichier à un BufferedReader et on recupoère la ligne sous forme de stream
## Exo 2
### Question 1
	* On peut les classer dans une collection
## Exo 3
### Question 4
	* Les tables de hachage permettent de recupérer une valeur a partri d'une clé
## Exo 4
### Question 4
	* On peut utiliser la classe Writer pour ecrire dans un fichier