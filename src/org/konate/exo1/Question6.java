package org.konate.exo1;

import java.util.Collections;
import java.util.List;

import org.konate.model.Country;

public class Question6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CountryAnalyzer countryToanalyze = new CountryAnalyzer();
		String fileName = "./files/data.txt";
		List<Country> countries = countryToanalyze.readCountries(fileName);
		Collections.sort(countries);
		System.out.println(countries);
	}

}
