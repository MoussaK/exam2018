package org.konate.exo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.konate.model.Country;

public class Question4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Country> countries = new ArrayList<>();
		countries.add(new Country("France"));
		countries.add(new Country("Espagne"));
		countries.add(new Country("Allemagne"));
		
		System.out.println("Avant le tris : " + countries);
		Collections.sort(countries);
		System.out.println("Après le tris : " + countries);
	}

}
