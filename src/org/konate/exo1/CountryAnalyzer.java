package org.konate.exo1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.konate.model.Country;

public class CountryAnalyzer {
	
	Predicate<String> isComment = s -> s.startsWith("#");
	Predicate<String> isNotEmpty = string -> !string.isEmpty();
	Function<String, String> lineToGroup = string -> string.substring(0, 1);
	Function<String, String> lineToTeamName = string -> string.substring(1)
															  .trim();
	Function<String, Country> stringToCountry = line -> new Country(lineToTeamName.apply(line), lineToGroup.apply(line));
	
	public List<Country> readCountries(String fileName) {
		File file = new File(fileName);
		List<Country> countries = new ArrayList<>();

		try (FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);) {

			countries = br.lines().filter(s -> !isComment.test(s) && isNotEmpty.test(s))
														 .map(stringToCountry)
														 .collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return countries;
	}
}
