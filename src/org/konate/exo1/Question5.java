package org.konate.exo1;

import java.util.function.Function;

import org.konate.model.Country;

public class Question5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Function<String, String> lineToGroup = string -> string.substring(0, 1);
		Function<String, String> lineToTeamName = string -> string.substring(1)
																  .trim();
		
		Function<String, Country> stringToCountry = line -> new Country(lineToTeamName.apply(line),
																		lineToGroup.apply(line));
		System.out.println(stringToCountry.apply("A Mali"));
	}
}
