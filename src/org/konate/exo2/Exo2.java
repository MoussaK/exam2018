package org.konate.exo2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.konate.exo1.CountryAnalyzer;
import org.konate.model.Country;

public class Exo2 {
	
	private static String fileName = "./files/data.txt";
	public static List<String> getCountryNamesFromGroup(String group) {
		//String fileName = "./files/data.txt";
		CountryAnalyzer countryToanalyze = new CountryAnalyzer();
		List<Country> countries = countryToanalyze.readCountries(fileName);
		Collections.sort(countries);
		Collection<Country> collectionCountries = countries;
		List<String> countryWhichMatchesGroup = new ArrayList<>();
		
		for (Country country : collectionCountries) {
			if(country.getGroup().equals(group))
				countryWhichMatchesGroup.add(country.getName());
		}
		return countryWhichMatchesGroup;
	}
	
	public static List<String> getOpponents(String countryName) {
		//String fileName = "./files/data.txt";
		CountryAnalyzer countryToanalyze = new CountryAnalyzer();
		List<Country> countries = countryToanalyze.readCountries(fileName);
		Collections.sort(countries);
		Collection<Country> collectionCountries = countries;
		String group = null;
		for (Country country : collectionCountries) {
			if(country.getName().equals(countryName))
				group = country.getGroup();
		}
		List<String> opponents = getCountryNamesFromGroup(group);
		opponents.remove(countryName);
		
		return opponents;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String fileName = "./files/data.txt";
		CountryAnalyzer countryToanalyze = new CountryAnalyzer();
		List<Country> countries = countryToanalyze.readCountries(fileName);
		Collections.sort(countries);
		//Collection<Country> collectionCountries = countries;
		
		System.out.println("Group A => " + getCountryNamesFromGroup("A"));
		System.out.println("Group F => " + getCountryNamesFromGroup("F"));
		System.out.println("Opposants Mexique => " + getOpponents("Mexique"));
		System.out.println("Opposants Russie => " + getOpponents("Russie"));
	}

}
