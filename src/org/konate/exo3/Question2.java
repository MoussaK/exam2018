package org.konate.exo3;

import java.util.ArrayList;
import java.util.List;

import org.konate.exo1.CountryAnalyzer;
import org.konate.exo2.Exo2;
import org.konate.model.Country;
import org.konate.model.Group;
import org.konate.model.Match;

public class Question2 {
	
	public static List<Match> getAllMaches(List<Group> groups) {
		List<Match> listOfAllMaches = new ArrayList<>();
		for (Group group : groups) {
			List<String> tmp = Exo2.getCountryNamesFromGroup(group.getGroup());
			for (int i = 0; i < tmp.size(); i++) {
				Country countryA = new Country(tmp.get(i));
				for (int j = 1 + i; j < 4; j++) {
					Country countryB = new Country(tmp.get(j));
					listOfAllMaches.add(new Match(countryA, countryB));
				}
			}
		}
		return listOfAllMaches;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "./files/data.txt";
		
		CountryAnalyzer countryToanalyze = new CountryAnalyzer();
		List<Country> countries = countryToanalyze.readCountries(fileName);
		List<String> group = new ArrayList<>();
		for (Country country : countries) {
			if(!group.contains(country.getGroup()))
				group.add(country.getGroup());
		}
		
		System.out.println(countries);
		System.out.println(group);
		
		Group newGroup = new Group("F");
		System.out.println("Liste des matchs : " + newGroup.getMaches());
		List<Group> groups = new ArrayList<>();
		for (String item : group) {
			groups.add(new Group(item));
		}
		System.out.println(getAllMaches(groups));
	}

}
