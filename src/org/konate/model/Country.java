package org.konate.model;

public class Country implements Comparable<Country>{
	
	private String name;
	private String group;
	public Country() {
		super();
	}
	public Country(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	public Country(String name, String group) {
		super();
		this.name = name;
		this.group = group;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Country [name=");
		builder.append(name);
		builder.append(", group=");
		builder.append(group);
		builder.append("]");
		return builder.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public int compareTo(Country o) {
		// TODO Auto-generated method stub
		return this.name.compareTo(o.name);
	}
	
	
}
