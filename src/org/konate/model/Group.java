package org.konate.model;

import java.util.ArrayList;
import java.util.List;

import org.konate.exo2.Exo2;

public class Group {
	
	private String group;
	private List<Country> countries;
	
	public Group(String group) {
		super();
		this.group = group;
	}
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public List<Country> getCountries() {
		return countries;
	}
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}
	
	public void addCountry(Country country){
		this.countries.add(country);
	}
	
	public List<Match> getMaches() {
		List<String> tmp = Exo2.getCountryNamesFromGroup(this.group);
		List<Match> listOfMatch = new ArrayList<>();
		for (int i = 0; i < tmp.size(); i++) {
			Country countryA = new Country(tmp.get(i));
			for (int j = 1 + i; j < 4; j++) {
				Country countryB = new Country(tmp.get(j));
				listOfMatch.add(new Match(countryA, countryB));
			}
		}
		return listOfMatch;
	}
	
	/*@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		builder.append(builder);
		return builder.toString();
	}*/
	
	
}
