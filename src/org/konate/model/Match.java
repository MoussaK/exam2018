package org.konate.model;

public class Match {
	
	private Country countryA, countryB;
	private int scoreA, scoreB;
	private String group;
	
	public Match(Country countryA, Country countryB) {
		super();
		this.countryA = countryA;
		this.countryB = countryB;
	}
	public Country getCountryA() {
		return countryA;
	}
	public void setCountryA(Country countryA) {
		this.countryA = countryA;
	}
	public Country getCountryB() {
		return countryB;
	}
	public void setCountryB(Country countryB) {
		this.countryB = countryB;
	}
	public int getScoreA() {
		return scoreA;
	}
	public void setScoreA(int scoreA) {
		this.scoreA = scoreA;
	}
	public int getScoreB() {
		return scoreB;
	}
	public void setScoreB(int scoreB) {
		this.scoreB = scoreB;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Match [");
		builder.append(countryA);
		builder.append("-");
		builder.append(countryB);
		builder.append(",");
		builder.append(scoreA);
		builder.append("-");
		builder.append(scoreB);
		builder.append("]");
		return builder.toString();
	}
	
	
}
